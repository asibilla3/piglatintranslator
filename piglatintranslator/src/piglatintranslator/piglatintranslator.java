package piglatintranslator;

public class piglatintranslator {

	public static final String NIL = "nil";

	private String phrase;

	public piglatintranslator(String inputPhrase)throws PigLatinException {

		int counterOfSpaces=0;
		int counterOfDashes=0;

		for(int i=0;i<inputPhrase.length();i++) {
			

			if(inputPhrase.charAt(i)==' ') {

				counterOfSpaces++;

				if(counterOfSpaces==2) {
					

					throw new PigLatinException("non puoi inserire piu' di due spazi");
				}


			}else {

				counterOfSpaces=0;
			}


			if(inputPhrase.charAt(i)=='-') {

				counterOfDashes++;

				if(counterOfDashes==2) {
				
					throw new PigLatinException("non puoi inserire piu' di due trattini");
				}


			}else {

				counterOfDashes=0;
			}



		}
		phrase= inputPhrase ;
		

	}

	public String getPhrase () {

		return this.phrase;
	}


	public String translate() {

		boolean checkSpace=true;
		boolean checkUnderscore=true;
		int positionEnd=0;
		String remainingPhrases=this.getPhrase();
		String translatedPhrase="";



		if(!(this.getPhrase().isEmpty())) {

			while(((remainingPhrases.indexOf("-")!=-1)||(remainingPhrases.indexOf(" ")!=-1))) {

				//System.out.println("prima della cond: "+positionStart+" "+positionEnd);

				if( (remainingPhrases.indexOf("-"))!=-1 ) {

					if((remainingPhrases.indexOf("-")<remainingPhrases.indexOf(" ") ) || checkSpace==false) {

						positionEnd= remainingPhrases.indexOf("-");

						translatedPhrase+=translateWord(remainingPhrases.substring(0,positionEnd))+'-';


						remainingPhrases= remainingPhrases.substring(positionEnd+1);

					}

				}else {

					checkUnderscore=false;
				}

				if((remainingPhrases.indexOf(" "))!=-1) {

					if(remainingPhrases.indexOf(" ")<remainingPhrases.indexOf("-")|| checkUnderscore==false ) {

						positionEnd= remainingPhrases.indexOf(" ");

						//System.out.println("dopo la cond: "+positionStart+" "+positionEnd);


						translatedPhrase+=translateWord(remainingPhrases.substring(0,positionEnd))+" ";


						remainingPhrases= remainingPhrases.substring(positionEnd+1);
					}

				}else {
					checkSpace=false;
				}

				System.out.println("frase tradotta :"+translatedPhrase);


				System.out.println("frase rimanente :"+remainingPhrases);


			}

			translatedPhrase+=translateWord(remainingPhrases);



			return translatedPhrase;

		}else {


			return piglatintranslator.NIL;
		}


	}

	public String translateWord(String word) {

		int indexOfPunctuations=-1;
		char savePunctuation = 0;

		if( this.getIndexOfPunctuations(word)!=-1) {

			indexOfPunctuations=this.getIndexOfPunctuations(word);
			savePunctuation=word.charAt(indexOfPunctuations);
			word=word.substring(0,indexOfPunctuations)+ word.substring(indexOfPunctuations+1);

			System.out.println(indexOfPunctuations);
			System.out.println(savePunctuation);

		}

		if(startingWithVowel(word)) {

			if(endingWithY(word) ){


				if(indexOfPunctuations!=-1)
					return (word+"nay"+savePunctuation)  ;
				else
					return (word+"nay")  ;

			}else if (endingWithVowel(word)){

				if(indexOfPunctuations!=-1)
					return (word+"yay"+savePunctuation)  ;
				else
					return (word+"yay");


			}else {

				if(indexOfPunctuations!=-1)
					return (word+"ay"+savePunctuation)  ;
				else
					return (word+"ay") ;

			}

		}else{

			int i=0;


			do{
				word+=word.charAt(i);	

				i++;


			}while(word.charAt(i)!='a'&& word.charAt(i)!='e'&& word.charAt(i)!='i'&& word.charAt(i)!='o'&& word.charAt(i)!='u');

			if(indexOfPunctuations!=-1)
				return word.substring(i)+"ay"+savePunctuation;
			else
				return word.substring(i)+"ay"  ;


		}

	}

	public boolean startingWithVowel(String word) {


		return (word.startsWith("a")||word.startsWith("e")||word.startsWith("i")||word.startsWith("o")||word.startsWith("u"));


	}



	public boolean endingWithY(String word) {

		return (word.endsWith("y"));


	}


	public boolean endingWithVowel(String word) {

		return (word.endsWith("a")||word.endsWith("e")||word.endsWith("i")||word.endsWith("o")||word.endsWith("u"));


	}


	public int getIndexOfPunctuations(String word) {

		String punctuations="',?():!.;";

		for (int i=0;i<punctuations.length();i++) {


			if( (word.indexOf(punctuations.charAt(i))) !=-1) {

				return word.indexOf(punctuations.charAt(i));
			}



		}

		return -1;


	}






}
