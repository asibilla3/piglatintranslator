package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class piglatintranslator_test {

	
	@Test
	public void testInputPhrase() throws Exception {
		String inputPhrase="hello world";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

	}

	@Test
	public void testTranslateNullInputPhrase() throws Exception {

		String inputPhrase="";
		piglatintranslator translator= new piglatintranslator(inputPhrase);


		assertEquals(translator.NIL,translator.translate()); 
	}


	@Test
	public void testTranslateWordStartingVowelWithEndingY() throws Exception {

		String inputPhrase="eay";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("eaynay",translator.translate()); 
	}



	@Test
	public void testTranslateWordStartingVowelWithEndingVowel() throws Exception {

		String inputPhrase="aiao";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("aiaoyay",translator.translate()); 
	}



	@Test
	public void testTranslateWordStartingVowelWithEndingConsonant() throws Exception {

		String inputPhrase="aiad";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("aiaday",translator.translate()); 
	}


	@Test
	public void testTranslateWordStartingWithOneConsonant() throws Exception {

		String inputPhrase="hello";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("ellohay",translator.translate()); 
	}



	@Test
	public void testTranslateWordStartingWithMoreConsonant() throws Exception{

		String inputPhrase="hhello";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("ellohhay",translator.translate()); 
	}


	@Test
	public void testTranslatePhraseWithDashes() throws Exception {
		String inputPhrase="hello-james-from-columbia";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("ellohay-amesjay-omfray-olumbiacay",translator.translate()); 
	}
	
	
	@Test
	public void testTranslatePhraseWithSpaces() throws Exception {
		String inputPhrase="hello james from columbia";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("ellohay amesjay omfray olumbiacay",translator.translate()); 
	}
	

	@Test
	public void testTranslatePhraseWithDashesAndSpaces() throws Exception {
		String inputPhrase="hello james from-columbia";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("ellohay amesjay omfray-olumbiacay",translator.translate()); 
	}
	

	@Test
	public void testTranslatePhraseWithPunctuations() throws Exception{
		String inputPhrase="hello james from-columbia!";
		piglatintranslator translator= new piglatintranslator(inputPhrase);

		assertEquals("ellohay amesjay omfray-olumbiacay!",translator.translate()); 
	}
	
	
	@Test(expected = PigLatinException.class)
	public void testTranslatePhraseWithMoreConsecutiveSeparators() throws PigLatinException {
		
		String inputPhrase="hello  james from-columbia!";
		piglatintranslator translator= new piglatintranslator(inputPhrase);
		 
		translator.translate(); 
	}
	
	
	





}
